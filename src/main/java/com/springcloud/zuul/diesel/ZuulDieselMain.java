package com.springcloud.zuul.diesel;

import com.springcloud.zuul.diesel.fallback.CommonFallbackProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.route.ZuulFallbackProvider;
import org.springframework.context.annotation.Bean;

/**
 * @Author: diesel
 * @Description:
 * @Date: Created in 2018/1/22 ${Time}.
 * @Modified : By [author] in [date]
 */
@SpringCloudApplication //@SpringCloudApplication注解整合了多个注解，@SpringBootApplication、@EnableDiscoveryClient、@EnableCircuitBreaker
@EnableZuulProxy
@EnableHystrixDashboard
public class ZuulDieselMain {
    public static void main(String[] args) {
        SpringApplication.run(ZuulDieselMain.class, args);
    }

    //    @Bean// 启用正则方式路由
//    public PatternServiceRouteMapper serviceRouteMapper() {
//        return new PatternServiceRouteMapper(
//                "(?<name>^.+)-(?<version>v.+$)",
//                "${name}/");
//        //  return new PatternServiceRouteMapper("(?<name>^)","${name}/");
//    }

    @Bean//启用熔断器
    public ZuulFallbackProvider routeAPIAZuulFallbackProvider() {
        CommonFallbackProvider routeZuulFallback = new CommonFallbackProvider();
        return routeZuulFallback;
    }
}
