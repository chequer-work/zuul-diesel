package com.springcloud.zuul.diesel.fallback;

import org.springframework.cloud.netflix.zuul.filters.route.ZuulFallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Author: diesel
 * @Description: 公用熔断器
 * @Date: Created in 2018/1/23 ${Time}.
 * @Modified : By [author] in [date]
 */
@Component
public class CommonFallbackProvider implements ZuulFallbackProvider {

    private String responseBody = "{\"message\":\"Service Unavailable. Please try after sometime\"}";
    private HttpHeaders headers = null;
    private String route = null;
    private int rawStatusCode = 503;
    private HttpStatus statusCode = HttpStatus.SERVICE_UNAVAILABLE;
    private String statusText = "Service Unavailable";

    @Override
    public String getRoute() {
        if (route == null)
            route = "*";    //  若为空, 则熔断默认route ID="route", 但我配置文件没配这个这只是一种实现的样例.
        return route;
    }

    /**
     * 熔断器会调用下面的fallbackResponse方法,最终模拟返回一个ClientHttpResponse.
     * 里面包含HttpHeaders、rawStatusCode、statusCode和responseBody等信息,这些信息都可以自定义返回值.
     * MediaType则包含多种返回信息的格式Json、Pdf、Image等等.
     *
     * @see org.springframework.cloud.netflix.zuul.filters.route.ZuulFallbackProvider#fallbackResponse()
     */
    @Override
    public ClientHttpResponse fallbackResponse() {
        return new ClientHttpResponse() {

            @Override
            public InputStream getBody() throws IOException {
                if (responseBody == null)
                    responseBody = "{\"message\":\"Service Unavailable. Please try after sometime\"}";
                return new ByteArrayInputStream(responseBody.getBytes());
            }

            @Override
            public HttpHeaders getHeaders() {
                if (headers == null) {
                    headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                }
                return headers;
            }

            @Override
            public void close() {

            }

            @Override
            public int getRawStatusCode() throws IOException {
                return rawStatusCode;
            }

            @Override
            public HttpStatus getStatusCode() throws IOException {
                if (statusCode == null)
                    statusCode = HttpStatus.SERVICE_UNAVAILABLE;
                return statusCode;
            }

            @Override
            public String getStatusText() throws IOException {
                if (statusText == null)
                    statusText = "Service Unavailable";
                return statusText;
            }

        };
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

    public void setHeaders(HttpHeaders headers) {
        this.headers = headers;
    }

    public int getRawStatusCode() {
        return rawStatusCode;
    }

    public void setRawStatusCode(int rawStatusCode) {
        this.rawStatusCode = rawStatusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public void setRoute(String route) {
        this.route = route;
    }

}
