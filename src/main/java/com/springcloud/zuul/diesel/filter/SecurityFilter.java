package com.springcloud.zuul.diesel.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: diesel
 * @Description:
 * @Date: Created in 2018/1/22 ${Time}.
 * @Modified : By [author] in [date]
 */
@Component
public class SecurityFilter extends ZuulFilter {
    @Override
    public String filterType() {
        //前置过滤器
        return "pre";
    }

    @Override
    public int filterOrder() {
        //优先级，数字越大，优先级越低
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        //是否执行该过滤器，true代表需要过滤
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        //获取传来的参数securityToken
        Object accessToken = request.getParameter("securityToken");
        if (accessToken == null) {

            //过滤该请求，不往下级服务去转发请求，到此结束
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(403);
            ctx.setResponseBody("{\"result\":\"securityToken is empty!\"}");
            return null;
        }
        //如果有token，则进行路由转发
        //这里return的值没有意义，zuul框架没有使用该返回值
        return null;
    }
}


